﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace hangfire_wepapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HangFireController : ControllerBase
    {


        //Fire & Forget job
        [HttpPost]
        [Route("[action]")]
        public IActionResult Welcome()
        {
            var jobId = BackgroundJob.Enqueue(() => SendWelcomeEmail("Welcome to our app"));

            return Ok($"Job Id:{jobId},Welcome email sent to the user");
        }

        //Delayed Job
        [HttpPost]
        [Route("[action]")]
        public IActionResult Discount()
        {
            int timeInSeconds = 30;
            var jobId = BackgroundJob.Schedule(() => SendWelcomeEmail("Welcome to our app"), TimeSpan.FromSeconds(30));
            return Ok($"Job Id:{jobId}, Discount email will be sent in {timeInSeconds} seconds.");
        }


        //Recurring (recursively)
        [HttpPost]
        [Route("[action]")]
        public IActionResult DatabaseUpdate()
        {
            RecurringJob.AddOrUpdate(() => Console.WriteLine("Database Updated."), Cron.Minutely);
            return Ok($"Database check job initiated.");
        }

        //Continous job
        [HttpPost]
        [Route("[action]")]
        public IActionResult Confirm()
        {
            var parentJobId = BackgroundJob.Schedule(() => SendWelcomeEmail("You asked to be Unsubscribed!"), TimeSpan.FromSeconds(30));
            BackgroundJob.ContinueJobWith(parentJobId, () => Console.WriteLine("You were unsubscribed"));
           
            return Ok("Confirmation job created");
        }
        public void SendWelcomeEmail(string text)
        {
            Console.WriteLine(text);
        }
    }
}
